package com.demo.ecart.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.demo.ecart.dto.ProductPriceResponseDto;
import com.demo.ecart.entity.ProductPriceDetails;
import com.demo.ecart.repository.ProductPriceRepository;
import com.demo.ecart.service.ProductPriceService;

class ProductPriceControllerTest {
	@InjectMocks
	ProductPriceController productPriceController;

	@Mock
	ProductPriceService productPriceService;

	@Mock
	ProductPriceRepository productPriceRepository;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSearchProductPriceByProductCode() throws Exception {
		ProductPriceDetails productPriceDetails = new ProductPriceDetails();
		productPriceDetails.setProductCode("HP_111");
		productPriceDetails.setProductPrice((double) 50000);
		ProductPriceResponseDto productPriceResponseDto = new ProductPriceResponseDto();
		productPriceResponseDto.setProductPrice(productPriceDetails.getProductPrice());
		when(productPriceRepository.findByProductCode("HP_111")).thenReturn(productPriceDetails);
		when(productPriceService.getProductPriceByProductCode(productPriceDetails.getProductCode())).thenReturn(productPriceResponseDto);
		productPriceController.getProductPriceByProductCode(productPriceDetails.getProductCode());
		verify(productPriceService, times(1)).getProductPriceByProductCode(productPriceDetails.getProductCode());
	}

}
