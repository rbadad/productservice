package com.demo.ecart.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.demo.ecart.dto.ProductPriceResponseDto;
import com.demo.ecart.entity.ProductPriceDetails;
import com.demo.ecart.repository.ProductPriceRepository;

public class ProductPriceServiceTest {
	@InjectMocks
	ProductPriceService productPriceService;

	@Mock
	ProductPriceRepository productPriceRepository;

	@BeforeEach
	public void Setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testProductCodeSearch() throws Exception {
		ProductPriceDetails productPriceDetails = new ProductPriceDetails();
		productPriceDetails.setProductCode("HP_111");
		productPriceDetails.setProductPrice((double) 50000);
		when(productPriceRepository.findByProductCode("HP_111")).thenReturn(productPriceDetails);

		ProductPriceResponseDto productPriceResponseDto = productPriceService
				.getProductPriceByProductCode(productPriceDetails.getProductCode());

		assertEquals(50000, productPriceResponseDto.getProductPrice());
		verify(productPriceRepository, times(1)).findByProductCode("HP_111");
	}

}
