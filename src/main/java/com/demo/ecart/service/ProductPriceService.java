package com.demo.ecart.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.ecart.dto.ProductPriceResponseDto;
import com.demo.ecart.entity.ProductPriceDetails;
import com.demo.ecart.repository.ProductPriceRepository;

@Service
public class ProductPriceService {

	@Autowired
	ProductPriceRepository productPriceRepository;

	public ProductPriceResponseDto getProductPriceByProductCode(String productCode) throws Exception {
		ProductPriceResponseDto productPriceResponseDto = new ProductPriceResponseDto();
		ProductPriceDetails productPriceDetails = productPriceRepository.findByProductCode(productCode);
		if (Objects.isNull(productPriceDetails)) {
			throw new Exception("Product Price Not Found For This Product Code");

		}
		productPriceResponseDto.setProductPrice(productPriceDetails.getProductPrice());
		return productPriceResponseDto;
	}
}
