package com.demo.ecart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.demo.ecart.entity.ProductPriceDetails;

@Repository
public interface ProductPriceRepository extends JpaRepository<ProductPriceDetails, Long> {

	public ProductPriceDetails findByProductCode(String productCode);
}
