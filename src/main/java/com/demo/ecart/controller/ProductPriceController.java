package com.demo.ecart.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.ecart.dto.ProductPriceResponseDto;
import com.demo.ecart.service.ProductPriceService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/products")
public class ProductPriceController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductPriceController.class);
	@Autowired
	ProductPriceService productPriceService;

	@ApiOperation("Search Product Price based on Product Code")
	@GetMapping("/getProductPrice/{productCode}")
	public ResponseEntity<ProductPriceResponseDto> getProductPriceByProductCode(@PathVariable("productCode") String productCode)
			throws Exception {
		LOGGER.info("****Inside ProductPriceController search() method ****");
		LOGGER.info("search() method called by productCode");
		ProductPriceResponseDto productPriceResponseDto = productPriceService.getProductPriceByProductCode(productCode);
		return new ResponseEntity<>(productPriceResponseDto, HttpStatus.OK);
	}

}
